import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import App from './App';
import HomePage from './pages/HomePage';
import About from './pages/About';
import Contact from './pages/Contact';
import Tarot from './pages/Tarot';
import Music from './pages/Music';

export default class Root extends Component {
/** Aici o sa pui basename in cazul in care nu merge ( relative path to the website) */ 
  render() {
    return (
      <BrowserRouter> 
        <App>
          <Switch>
            <Route exact path='/' component={HomePage}></Route>
            <Route path='/about' component={About}></Route>
            <Route path='/contact' component={Contact}></Route>
            <Route path='/tarot' component={Tarot}></Route>
            <Route path='/music' component={Music}></Route>
          </Switch>
        </App>
      </BrowserRouter>
    )
  }
}
