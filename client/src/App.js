import React, { Component } from 'react';
import HeaderComponent from './components/App/HeaderComponent';
// import Footer from './components/App/Footer';
import { Layout } from 'antd';
import {withRouter} from 'react-router-dom';
const {  Footer, Content } = Layout;
const HeaderComp  = withRouter(props => <HeaderComponent {...props} />);
class App extends Component {
  render() {
    return ( 
    <Layout className="layout">
        <HeaderComp />
      <Content style={{minHeight:'500px'}}>
        {this.props.children}
      </Content>
      <Footer>This is the footer</Footer>
    </Layout>
    );
  }
}

export default App;
