import axios from "axios";
export const GET_MUSIC = "GET_MUSIC";

export function getMusic(music) {
  return {
    type: GET_MUSIC,
    payload: music.data
  };
}

export function fetchMusicList() {
  return dispatch => {
    getMusicAsync()
      .then(data => dispatch(getMusic(data)))
      .catch(err => console.log(err));
  };
}

async function getMusicAsync() {
  return await axios.get("/api/healingMusic");
}
