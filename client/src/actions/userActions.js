import axios from 'axios';


export const SET_USER = 'SET_USER';

export function setUser(user) {
  return {
    type: SET_USER,
    user
  }
}

export function fetchUser() {
  return dispatch => {
    axios.get('/api/auth/users/1')
    .then(data => dispatch(setUser(data)));
  }
}
