import React from "react";
import Sound from "react-sound";
import PlayerControls from "./musicPlayer/PlayerControls";

export default class Player extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentSong: this.props.currentSong,
      position: 0,
      volume: 100,
      duration: 0,
      playStatus: Sound.status.STOPPED
    };

    this.handleSeekBar = this.handleSeekBar.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (this.props.currentSong !== newProps.currentSong) {
      this.handleSongSelected(newProps.currentSong);
    }
  }
  convertToMilyseconds(time) {
    console.log(time);
    var mils = time.split(":");
    return mils[0] * 60000 + mils[1] * 1000;
  }

  handleSeekBar(e) {
    let elem = document.querySelectorAll(
      '[data-bar="play_bar_' + this.state.currentSong.id + '"]'
    )[0];
    let left = elem.getBoundingClientRect().left;
    let x = (e.pageX - left) / elem.parentNode.clientWidth * 100;
    this.setPosition(x);
  }

  _formatMilliseconds(milliseconds) {
    milliseconds = milliseconds % 3600000;
    let minutes = Math.floor(milliseconds / 60000);
    milliseconds = milliseconds % 60000;
    let seconds = Math.floor(milliseconds / 1000);
    milliseconds = Math.floor(milliseconds % 1000);

    return (
      (minutes < 10 ? "0" : "") +
      minutes +
      ":" +
      (seconds < 10 ? "0" : "") +
      seconds
    );
  }

  setPosition(percent) {
    let position = this.state.duration * percent / 100;
    this.setState({ position, playStatus: Sound.status.PLAYING });
  }

  getPosition(pos) {
    let seekBar = { width: "" };
    seekBar.width = this.state.position / this.state.duration * 100 + "%";
    return seekBar;
  }

  render() {
    const { volume } = this.state;

    return (
      <div className="audio">
        <PlayerControls
          playStatus={this.state.playStatus}
          onPlay={() => this.setState({ playStatus: Sound.status.PLAYING })}
          onPause={() => this.setState({ playStatus: Sound.status.PAUSED })}
          onResume={() => this.setState({ playStatus: Sound.status.PLAYING })}
          onStop={() =>
            this.setState({ playStatus: Sound.status.STOPPED, position: 0 })}
          onSeek={position => this.setState({ position })}
          onVolumeUp={() =>
            this.setState({ volume: volume >= 100 ? volume : volume + 10 })}
          onVolumeDown={() =>
            this.setState({ volume: volume <= 0 ? volume : volume - 10 })}
          duration={
            this.state.currentSong ? this.state.currentSong.duration : 0
          }
          position={this.state.position}
        />
        {this.state.currentSong && (
          <Sound
            url={this.state.currentSong.url}
            playStatus={this.state.playStatus}
            playFromPosition={this.state.position}
            volume={volume}
            onLoading={({ duration }) => {
              if (this.state.duration !== duration) this.setState({ duration });
            }}
            onPlaying={({ position }) => this.setState({ position })}
            onPause={() => console.log("Paused")}
            onResume={() => console.log("Resumed")}
            onStop={() => console.log("Stopped")}
            onFinishedPlaying={() =>
              this.setState({ playStatus: Sound.status.STOPPED })}
          />
        )}
        <div className="bar">
          <div className="seek-bar" onClick={this.handleSeekBar}>
            <div
              className="play-bar"
              data-bar={"play_bar_" + this.state.currentSong.id}
              style={this.getPosition(this.state.position)}
            />
            <div className="details">
              <span className="title">{this.state.currentSong.title}</span>
            </div>
            <div className="timing">
              <span className="duration">{this.state.currentSong.length}</span>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleSongSelected(song) {
    this.setState({ currentSong: song, position: 0 });
  }
}
