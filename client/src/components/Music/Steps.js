import React from "react";
import { Col, Row } from "antd";
import Player from './Player';
export default function Steps(props) {
   
  return (
      
        <div>
            {props.songs.map(song => {
                let songInfo = {
                    title: song.song_name,
                    url: song.url,
                    length: song.length,
                    id: song.id
                }   
                return (
                    <Row key={song.id}>
                        <Col xs={6}>
                            <div> Step 1 {song.title}</div>
                            <div><Player currentSong={songInfo} /></div>
                        </Col>
                        <Col xs={6}>
                            <p>{song.description}</p>
                        </Col>
                    </Row>
                )
            })}
        </div>

  );
}


