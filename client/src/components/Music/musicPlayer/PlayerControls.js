import React from "react";
import Sound from "react-sound";

export default class PlayerControls extends React.Component {
  render() {
    return <div>{this.renderControls()}</div>;
  }

  renderControls() {
    const controls = {
      play: this.props.playStatus === Sound.status.STOPPED,
      stop: this.props.playStatus !== Sound.status.STOPPED,
      pause: this.props.playStatus === Sound.status.PLAYING,
      resume: this.props.playStatus === Sound.status.PAUSED
    };

    return (
      <div>
        <div className="play-control control">
          {controls.play && (
            <span className="play button" onClick={this.props.onPlay} />
          )}
          {controls.pause && (
            <span className="pause button" onClick={this.props.onPause} />
          )}
          {controls.resume && (
            <span className="play button" onClick={this.props.onResume} />
          )}
        </div>
      </div>
    );
  }
}
