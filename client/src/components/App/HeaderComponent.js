import React, { Component } from 'react';
import { Menu, Icon  } from 'antd';
import { Link} from "react-router-dom";

class HeaderComponent extends Component {
  constructor(props) {
    super(props);
    this.expandMenu = this.expandMenu.bind(this);
    this.state = {
      isOpen: false,
      currentPath: '/',
      expanded: {
        right:'-300px'
      }
    };
  }
  expandMenu() {
      if(this.state.isOpen) {
        this.setState({expanded: {right:'-300px'}, isOpen: !this.state.isOpen });
      }
      else {
        this.setState({expanded: {right:'0'}, isOpen: !this.state.isOpen});
      }
  }

  render() {
    return (
      <div className="menu-slider-right" style={this.state.expanded}>
        <div className={ (!this.state.isOpen) ? 'menu-slider-button' : 'menu-slider-button-close'} onClick={this.expandMenu}>
          {!this.state.isOpen && <span><Icon type='bars' />Menu</span> }
          {this.state.isOpen && <Icon type='close' />}
        </div>
        <div className="menu-slider-children">
            <div className="logo">Zelnath</div>
              <Menu 
              theme="dark" 
              style={{lineHeight: '62px'}} 
              mode="vertical" 
              defaultSelectedKeys={['/']} 
              onSelect={this.expandMenu}
              >
                <Menu.Item key="/">
                  <Link to="/"><span className="nav-text">Home</span></Link>
                </Menu.Item>
                <Menu.Item key="/about">
                  <Link to="/about"><span className="nav-text">About</span></Link>
                </Menu.Item>
                <Menu.Item key="/music">
                  <Link to="/music">Music</Link>
                </Menu.Item>
                <Menu.Item key="/contact">
                  <Link to="/contact">Contact</Link>
                </Menu.Item>
              </Menu>
          </div>
      </div>
    );
  }
}

export default HeaderComponent;
