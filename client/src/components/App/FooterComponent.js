import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Row, Col } from "antd";

class FooterComponent extends Component {
  render() {
    return (
      <Row>
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/contact">Contact</Link>
      </Row>
    );
  }
}

export default Footer;
