import React from 'react';
import './slider.css';

export default function SliderKenBurns(props) {
 
  return(
    <div className="slideshow">
        {props.images.map((image, index) => {
           return (
               <div key={index} className="slideshow-image" style={{backgroundImage: `url(${image.url})`}}></div>
            )
        })}
    </div>
  )

}
