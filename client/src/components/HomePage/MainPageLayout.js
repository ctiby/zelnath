import React, { Component } from 'react';
import PropTypes from 'react-proptypes';
import { Link } from 'react-router-dom';
import { Row, Col } from 'antd';

class MainPageLayout extends Component {

    constructor(props) {
        super(props);

        this.state = { 
           
        }
    }

    ComponentDidMount() {
        console.log('aaaaaaaa s-a montat');
    }

    render() {
        return(
            <Row>
                <Col> 
                    <Link to={ (this.props.title === 'Astrological Tarot') ? '/tarot' : '/music' }>
                       {this.props.title}
                    </Link>
                </Col>
                <Col >
                    <div>Some sexy text goes here</div>
                </Col>
            </Row>
        )
    }
}
 MainPageLayout.propTypes = {
        title: PropTypes.string.isRequired
    }

    export default MainPageLayout;