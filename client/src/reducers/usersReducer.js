import { SET_USER } from '../actions/userActions';

export default function usersReducer(state=[], actions = {}) {

  switch(actions.type) {

    case SET_USER: return actions.user.data[0];

    default: return state;
  }


}
