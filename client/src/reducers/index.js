import { combineReducers } from 'redux';

import tweets from './tweetsReducer';
import users from './usersReducer';
import music from './musicReducer';


export default combineReducers({
  tweets,
  users,
  music
})
