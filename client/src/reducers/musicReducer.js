import { GET_MUSIC } from "../actions/musicActions";
const initialState = {
  musicDescriptions: [],
  songsList: []
};
export default function musicReducer(state = initialState, actions = {}) {
  switch (actions.type) {
    case GET_MUSIC:
      state = {
        ...state,
        musicDescriptions: actions.payload.musicDescriptions,
        songsList: actions.payload.songsList
      };
      return state;

      default: return state;
  }
}
