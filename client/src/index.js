import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./store";
import Root from "./routes";
// import './common/purple-style.css';
import './common/style.css';
import registerServiceWorker from './registerServiceWorker';

const app = document.getElementById("root");

ReactDOM.render(
  <Provider store={store}>
     <Root />
  </Provider>,
  app
);
registerServiceWorker();
