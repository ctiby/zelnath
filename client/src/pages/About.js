import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUser } from '../actions/userActions';
import PropTypes from 'react-proptypes';


class About extends Component {
  componentDidMount() {
    this.props.fetchUser();
  }
  componentWillMount() {
    
  }

  render() {
    const noUser = (
      <h1> About </h1>
    );
     const hasUser = (
       <h1>About {this.props.users['name']}</h1>
       );

    return (
      <div>
        { this.props.users.length === 0 ?  noUser : hasUser }
      </div>
    )
  }
}

About.propTypes = {
  users: PropTypes.array.isRequired,
  fetchUser: PropTypes.func.isRequired
}

function mapStateToProps(state) {

  return {
    users: state.users
   }
}

export default connect(mapStateToProps, { fetchUser })(About);
