import React, { Component } from "react";
import { Row, Col, Tabs } from "antd";
import { connect } from "react-redux";
import { fetchMusicList } from "../actions/musicActions";
import Steps2 from "../components/Music/Steps";
import PropTypes from "react-proptypes";
import { Steps } from 'antd';
const Step = Steps.Step;
const TabPane = Tabs.TabPane;
class Music extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "1",
      active: false
    };
    this.showDescription = this.showDescription.bind(this);
    this.handleOnClickBtn = this.handleOnClickBtn.bind(this);
  }

  componentDidMount() {
    this.props.fetchMusicList();
  }

  showDescription(id) {
    this.setState({
      selected: id,
      active: false
    });
  }

  getSongsList(id) {
    let songsList = [];
    this.props.music.songsList.map(song => {
      if (song["healingId"] === id) {
         songsList.push(song);
         return songsList;
      }
    });
    return songsList;
  }

  handleOnClickBtn() {
    this.setState({ active: !this.state.active });
  }

  render() {
    let songs = this.getSongsList(this.state.selected);
    return (
      <div>
        <Row>
          <div className="abstract">
            <h2>Change your mood with</h2>
            <h3>Zodiac Healing Music</h3>
          </div>
        </Row>

        <Row>
          <Col xl={8}>
            <Tabs tabPosition="left" onTabClick={this.showDescription}>
              {this.props.music.musicDescriptions.map(listaMuzica => {
                return (
                  <TabPane tab={listaMuzica["name"]} 
                          key={listaMuzica["id"]} 
                          onClick={this.showDescription}
                          >
                  <Col xs={20} sm={16} md={12} lg={8} xl={12}>
                    <p>
                      {listaMuzica["description"]}
                    </p>
                      <button
                        className="btn-site btn-color"
                        onClick={this.handleOnClickBtn}
                      >
                        Start Process
                      </button>
                      {this.state.active ? <Steps2 songs={songs} /> : ""}
                    </Col>
                  </TabPane>
                );
              })}
            </Tabs>
          </Col>
          <Col xl={4}>
          <Steps direction="vertical" size="small" current={1}>
            <Step title="Finished" description="This is a description." />
            <Step title="In Progress" description="This is a description." />
            <Step title="Waiting" description="This is a description." />
          </Steps>
          </Col>
        </Row>
      </div>
    );
  }
}

Music.PropTypes = {
  music: PropTypes.array.isRequired
};

function mapStateToProps(state) {
  return {
    music: state.music
  };
}

export default connect(mapStateToProps, { fetchMusicList })(Music);
