import React, { Component } from 'react';
import MainPageLayout from '../components/HomePage/MainPageLayout';
import { Row, Col } from 'antd';
import SliderKenBurns from '../components/HomePage/sliderKenBurns/Slider';

class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
       images: [
         {url: '/images/1.jpg'},
         {url: '/images/2.jpeg'},
         {url: '/images/3.jpeg'},
         {url: '/images/4.jpeg'},
      ]
    }
  }
  render() {
    return (
        <div className="container">
        <div className="homepage">
           <Row gutter={16}>
          <h1 className='home-header'>Zelnath - Online astrologer</h1>
          <Col span={12} className='box'><MainPageLayout title="Astrological Tarot" /></Col>
          <Col span={12}><MainPageLayout title="Zodia Healing Music" /></Col>
          </Row>
        </div>
        <SliderKenBurns images={this.state.images} />
      </div>
    )
  }
}

export default HomePage;
