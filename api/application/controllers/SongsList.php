<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class SongsList extends REST_Controller
{

       public function __construct() {
               parent::__construct();
               $this->load->model('songs_model');

       }
       public function index_get(){
					$id = $this->uri->segment(2);
                    $filter = array($id);
					if($id === NULL) {
						$r = $this->songs_model->read();
					} else {
						$r = $this->songs_model->readById($id);
					}
					$this->response($r);

       }

       public function index_put(){
           $id = $this->uri->segment(2);

           $data = array('name' => $this->input->get('name'),
           'description' => $this->input->get('pass'),
           'songs' => $this->input->get('type')
           );

            $r = $this->songs_model->update($id,$data);
               $this->response($r);
       }

       public function index_post(){
           $data = array('name' => $this->input->post('name'),
           'description' => $this->input->post('description'),
           'songs' => $this->input->post('songs')
           );
           $r = $this->songs_model->insert($data);
           $this->response($r);
       }
       public function index_delete(){
           $id = $this->uri->segment(2);
           $r = $this->songs_model->delete($id);
           $this->response($r);
       }


}
