<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Users extends REST_Controller
{

       public function __construct() {
               parent::__construct();
               $this->load->model('user_model');

       }
       public function index_get(){
					$id = $this->uri->segment(2);
					if($id === NULL) {
						$r = $this->user_model->read();
					} else {
						$r = $this->user_model->readById($id);
					}
					$this->response($r);

       }

       public function index_put(){
           $id = $this->uri->segment(2);

           $data = array('name' => $this->input->get('name'),
           'pass' => $this->input->get('pass'),
           'type' => $this->input->get('type')
           );

            $r = $this->user_model->update($id,$data);
               $this->response($r);
       }

       public function index_post(){
           $data = array('name' => $this->input->post('name'),
           'pass' => $this->input->post('pass'),
           'type' => $this->input->post('type')
           );
           $r = $this->user_model->insert($data);
           $this->response($r);
       }
       public function index_delete(){
           $id = $this->uri->segment(2);
           $r = $this->user_model->delete($id);
           $this->response($r);
       }


}
