<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class HealingMusic extends REST_Controller
{

       public function __construct() {
               parent::__construct();
               $this->load->model('music_model');

       }
       public function index_get(){
					$id = $this->uri->segment(2);
                    $filter = array($id);
					if($id === NULL) {
						$r = $this->music_model->getPage();
					} else {
						$r = $this->music_model->readById($id);
					}
					$this->response($r);

       }

       public function index_put(){
           $id = $this->uri->segment(2);

           $data = array('name' => $this->input->get('name'),
           'description' => $this->input->get('pass'),
           'songs' => $this->input->get('type')
           );

            $r = $this->music_model->update($id,$data);
               $this->response($r);
       }

       public function index_post(){
           $data = array('name' => $this->input->post('name'),
           'description' => $this->input->post('description'),
           'songs' => $this->input->post('songs')
           );
           $r = $this->music_model->insert($data);
           $this->response($r);
       }
       public function index_delete(){
           $id = $this->uri->segment(2);
           $r = $this->music_model->delete($id);
           $this->response($r);
       }


}
