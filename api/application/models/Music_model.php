<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Music_model extends CI_Model
{

public function getPage() {
    $music = [];
    $q1 = $this->db->query('SELECT DISTINCT m.id, m.name, m.description FROM healing_music m RIGHT JOIN songs s ON m.id = s.healingId');
    $q2 = $this->db->query('SELECT * FROM `songs`');

    $music['musicDescriptions'] = $q1->result_array();
    $music['songsList'] = $q2->result_array();
    return $music;
}


public function read(){
      $finalArray = [];
      $tempArray = [];
      $allSongs = [];
      $q = "SELECT DISTINCT m.id, m.name, m.description, s.song_name as title, s.url, s.length FROM healing_music m RIGHT JOIN songs s ON m.id = s.healingId";

       $query = $this->db->query($q);
       $result = $query->result_array();
       $tempName = '';  
       $i = 0;
       foreach( $result as $res ) {
           if($res['name'] != $tempName) {
               $tempName = $res['name'];
               $allSongs[] = array_splice($res, 3);
               $tempArray[$i] = array_splice($res, 0, 3);
               $tempArray[$i]['songs'] = $allSongs;
               $i++;
           }
           else
           {
                $allSongs[] = array_splice($res, 3);
                $tempArray[$i-1]['songs'] = array_merge($tempArray[$i-1]['songs'], $allSongs);
           }
           $allSongs = [];
       }
       return $tempArray;


   }

	 public function readById($id) {
		 $query = $this->db->query("select * from `healing_music` where `id`=$id");
		 return $query->result_array();
	 }

   public function insert($data){
       $this->user_name    = $data['name']; // please read the below note
       $this->user_password  = $data['description'];
       $this->user_type = $data['songs'];

       if($this->db->insert('healing_music',$this))
       {
           return 'Data is inserted successfully';
       }
         else
       {
           return "Error has occured";
       }
   }

   public function update($id,$data){
      $this->user_name    = $data['name']; // please read the below note
       $this->user_password  = $data['description'];
       $this->user_type = $data['songs'];
       $result = $this->db->update('healing_music',$this,array('id' => $id));
       if($result)
       {
           return "Data is updated successfully";
       }
       else
       {
           return "Error has occurred";
       }
   }

   public function delete($id){
       $result = $this->db->query("delete from `healing_music` where id = $id");

       if($result)
       {
           return "Data is deleted successfully";
       }
       else
       {
           return "Error has occurred";
       }
   }
}
